package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {

    @Required
    private String calificaciones;
    private String notaUno;
    private String notaDos;
    private String estado;
    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _calificaciones,
                String _notaUno,
                String _notaDos,
                String _estado)
    {
        this.calificaciones = _calificaciones;
        this.notaUno = _notaUno;
        this.notaDos = _notaDos;
        this.estado = _estado;
    }
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }
    public String getCalificaciones() { return calificaciones; }
    public String getNotaUno() { return notaUno; }
    public String getNotaDos() { return notaDos; }
    public String getEstado() { return estado; }
    public void setCalificaciones(String calificaciones) { this.calificaciones = calificaciones; }
    public void setNotaUno(String notaUno) { this.notaUno = notaUno; }
    public void setNotaDos(String notaDos) { this.notaDos = notaDos; }
    public void setEstado(String estado) { this.estado = estado; }



}
